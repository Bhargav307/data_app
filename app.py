from flask import Flask, jsonify, json

app = Flask(__name__)

users = '''[
        {"Name":"Sainath", "Skillset":"Python"},
        {"Name":"Gunasekar", "Skillset":"Java"},
        {"Name":"Bhargav", "Skillset":"Python"}
    ]'''

@app.route('/<req>')
def pick(req):
    data = json.loads(users)
    if req == "all":
        return jsonify(data)

    d=[]
    for i in range(len(data)):
        dic={}
        for j in data[i].keys():
            if j==req:
                dic[req]=data[i][j]
        d.append(dic)
    return jsonify(d)
if __name__ == '__main__':
    app.run(debug=True, port=5001)

